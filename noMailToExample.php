<?php

/**
 * noMailToExample : just don't send email to example.org or example.com
 * http://example.org/ is a great tool for demonstration and test, but sending an email to user@example.org: you receive 4 hour after a notification
 * This plugin just disable sending email to this website, then you can use it when testing syste.
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2016-2024 Denis Chenu <http://www.sondages.pro>
 * @license MIT
 * @version 1.1.0
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the “Software”), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
class noMailToExample extends PluginBase
{
    protected static $description = 'Don\t send email to example.(com|org)';
    protected static $name = 'noMailToExample';

    public function init()
    {
        $this->subscribe('beforeTokenEmail');
    }

    /**
     * Set event send to false when sending an email to example.(com|org)
     * @link https://manual.limesurvey.org/BeforeTokenEmail
     */
    public function beforeTokenEmail()
    {
        $emailTos = $this->event->get("to");
        if (empty($emailTos)) {
            return;
        }
        if (is_string($emailTos)) {
            $emailTos = array($emailTos);
        }
        /* @var string[] no example.(org|com) from the list */
        $cleanedEmailTos = array();
        foreach ($emailTos as $emailTo) {
            if (is_array($emailTo)) {
                $emailOnly = trim($emailTo[0]);
            } else {
                /* 3.X compatibility */
                if (strpos($emailTo, '<')) {
                    $emailOnly = trim(substr($emailTo, strpos($emailTo, '<') + 1, strpos($emailTo, '>') - 1 - strpos($emailTo, '<')));
                } else {
                    $emailOnly = trim($emailTo);
                }
            }
            /* @var string only domain from email */
            $domainName = strtolower(substr(strrchr($emailOnly, "@"), 1));
            if ($domainName == 'example.com' || $domainName == 'example.org') {
                /* temporary set send to false : deactivate sending email on all emails */
                $this->event->set("send", false);
            } else {
                $cleanedEmailTos[] = $emailTo;
            }
        }
        /* If we have a list of email with some example.(org|com) and other : set new list to cleaned list */
        if ($this->event->get("send", true) === false && !empty($cleanedEmailTos)) {
            $this->event->set("send", true);
            $this->event->set("to", $cleanedEmailTos);
        }
    }
}
